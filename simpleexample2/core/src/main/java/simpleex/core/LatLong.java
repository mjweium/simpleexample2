package simpleex.core;

/**
 * Represents a geo-location as a latitude, longitude pair.
 */
public class LatLong {

  /**
   * The default separator for the valueOf method.
   */
  public static final String SEPARATOR = ",";

  private final double latitude;
  private final double longitude;

  private MetaData metaData;

  /**
   * Initializes this LatLong with the provided latitued and longitude.
   *
   * @param latitude the latitude
   * @param longitude the longitude
   */
  public LatLong(final double latitude, final double longitude) {
    super();
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  @Override
  public String toString() {
    return latitude + SEPARATOR + longitude;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    long temp = Double.doubleToLongBits(latitude);
    int result = prime + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(longitude);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final LatLong other = (LatLong) obj;
    return (Double.doubleToLongBits(latitude) == Double.doubleToLongBits(other.latitude)
        && Double.doubleToLongBits(longitude) == Double.doubleToLongBits(other.longitude));
  }

  /**
   * Creates a LatLong object from a String.
   * The format is &lt;latitude&gt;,&lt;longitude&gt;.
   *
   * @param s the String to parse
   * @return the new LatLong object
   */
  public static LatLong valueOf(final String s) {
    return valueOf(s, SEPARATOR);
  }

  /**
   * Creates a LatLong object from a String, using a specific separator.
   * The format is &lt;latitude&gt;&lt;separator&gt;&lt;longitude&gt;.
   *
   * @param s the String to parse
   * @param sep the separator
   * @return the new LatLong object
   */
  public static LatLong valueOf(final String s, final String sep) {
    final int pos = s.indexOf(sep);
    if (pos < 0) {
      throw new IllegalArgumentException("No '" + sep + "' in " + s);
    }
    final double lat = Double.valueOf(s.substring(0, pos).trim());
    final double lon = Double.valueOf(s.substring(pos + sep.length()).trim());

    return new LatLong(lat, lon);
  }

  /**
   * This routine calculates the distance between two points (given the
   * latitude/longitude of those points). It is being used to calculate
   * the distance between two locations using GeoDataSource (TM) products
   *
   * <p>Definitions:
   * South latitudes are negative, east longitudes are positive
   *
   * <p>Passed to function:
   * lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)
   * lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)
   * Worldwide cities and other features databases with latitude longitude
   * are available at http://www.geodatasource.com
   *
   * <p>For enquiries, please contact sales@geodatasource.com
   *
   * <p>Official Web site: http://www.geodatasource.com
   *
   * <p>GeoDataSource.com (C) All Rights Reserved 2015
   *
   * @param lat1 the latitude of the first point
   * @param lon1 the longitude of the first point
   * @param lat2 the latitude of the other point
   * @param lon2 the longitude of the other point
   * @return
   */
  public static double distance(final double lat1, final double lon1, final double lat2,
      final double lon2) {
    if (lon1 == lon2 && lat1 == lat2) {
      return 0.0;
    }
    final double theta = lon1 - lon2;
    double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
        + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
    dist = Math.acos(dist);
    // convert to degrees
    dist = rad2deg(dist);
    dist = dist * 60 * 1.1515;
    // convert to meters
    dist = dist * 1609.344;
    return dist;
  }

  /**
   * Computes the distance in meters between two LatLong objects.
   *
   * @param latLong1 the first LatLong
   * @param latLong2 the other LatLong
   * @return the distance in meters
   */
  public static double distance(final LatLong latLong1, final LatLong latLong2) {
    return distance(latLong1.latitude, latLong1.longitude, latLong2.latitude, latLong2.longitude);
  }

  /**
   * Computes the distance in meters between this LatLong and an other one.
   *
   * @param latLong2 the other LatLong
   * @return the distance in meters
   */
  public double distance(final LatLong latLong2) {
    return distance(latitude, longitude, latLong2.latitude, latLong2.longitude);
  }

  /**
   * This function converts decimal degrees to radians.
   *
   * @param deg the desimal degrees
   * @return the corresponding radians
   */
  private static double deg2rad(final double deg) {
    return (deg * Math.PI / 180.0);
  }

  /**
   * This function converts radians to decimal degrees.
   *
   * @param rad the radians
   * @return the corresponding decimal degrees
   */
  private static double rad2deg(final double rad) {
    return (rad * 180 / Math.PI);
  }

  /**
   * Checks if this object has meta data.
   *
   * @return true if this LatLong object has meta data, false otherwise
   */
  public boolean hasMetaData() {
    return metaData != null && (! metaData.isEmpty());
  }

  /**
   * Gets the meta data of this object. Will create it if missing,
   * so it will always be safe to chain a call.
   *
   * @return the meta data of this object
   */
  public MetaData getMetaData() {
    if (metaData == null) {
      metaData = new MetaData();
    }
    return metaData;
  }
}
