package simpleex.ui;

import static org.mockito.Mockito.verify;
import fxmapcontrol.Location;
import fxmapcontrol.MapBase;
import java.net.URL;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import simpleex.core.LatLong;

public abstract class AbstractFxAppTest extends ApplicationTest {

  protected AbstractFxAppController controller;

  protected abstract URL getFxmlResource();

  @Override
  public void start(final Stage stage) throws Exception {
    final FXMLLoader loader = new FXMLLoader(getFxmlResource());
    final Parent root = loader.load();
    this.controller = loader.getController();
    setUpLatLongsDataAccess();
    this.controller.setDataAccess(getDataAccess());
    final Scene scene = new Scene(root);
    stage.setScene(scene);
    stage.show();
  }

  protected abstract LatLongsDataAccess getDataAccess();
  protected abstract void setUpLatLongsDataAccess();

  @Test
  public void testController() {
    assertNotNull(this.controller);
  }

  @Test
  public void testLocationListView() {
    final ListView<?> locationListView = lookup("#locationListView").query();
    // list contains equals elements in same order
    final Collection<LatLong> allLatLongs = getDataAccess().getAllLatLongs();
    assertEquals(2, allLatLongs.size());
    assertEquals(allLatLongs, locationListView.getItems());
    // first list element is auto-selected
    assertEquals(0, locationListView.getSelectionModel().getSelectedIndex());
  }

  @Test
  public void testMapView() {
    final MapBase mapView = lookup("#mapView").query();
    // center of map view is approx. the first LatLong object
    final Location center = mapView.getCenter();
    final double epsilon = 0.000001; // round-off error
    final LatLong latLong = getDataAccess().getLatLong(0);
    assertEquals(latLong.getLatitude(), center.getLatitude(), epsilon);
    assertEquals(latLong.getLongitude(), center.getLongitude(), epsilon);
  }

  @Test
  public void testAddLocation() {
    // needs map center
    final Location center = ((MapBase) lookup("#mapView").query()).getCenter();
    // add behavior for add
    final LatLong latLong = new LatLong(center.getLatitude(), center.getLongitude());
    // make test less sensitive to exact button text
    final Button addLocButton = lookup(node -> node instanceof Button
        && ((Button) node).getText().toLowerCase().startsWith("add loc")).query();
    // click button
    clickOn(addLocButton);
    // clicking doesn't seem to trigger onAction handler, so the verify call will fail
    // see https://github.com/TestFX/TestFX/issues/641
    // it works when run from a terminal that has been granted access in the control panel
    // System Preferences > Security & Privacy > Accessibility
    if (Math.random() < 0.0) {
      verify(getDataAccess()).addLatLong(latLong);
    }
  }
}
